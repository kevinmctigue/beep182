# Beep182

A car backup sensor written in AVR assembly

## Supplies

This project uses:

* JSN-SR04T ultrasonic sensor
* Arduino Nano Every (the old Arduino Nano won't work because the assembly was written for the Atmega4809)
* A piezo buzzer

## Wiring

If you are looking down from the top so that you can see the 4809 chip, built-in LEDs, etc...

```
               ____=======____
              |               |
              o               o
Piezo VCC ----o (3.3v)        o
Piezo SIG ----o (A0/D14)      o
              o               o
              o               o
              o               o
              o               o
              o               o
              o               o
              o               o
              o          (D3) o---- Sensor send
Sensor 5v ----o (5v)     (D2) o---- Sensor response
              o         (GND) o---- Sensor GND
Piezo GND ----o (GND)         o
              o               o
              |               |
               _______________
```

The sensor will pulse high (in microseconds) on D2 based on how far the object is from the sensor. That number is then divided into a more reasonable number of milliseconds to delay between beeps.

## Uploading

Uploading requires `avr-gcc` and the [arduino-cli](https://github.com/arduino/arduino-cli).

```sh
avr-gcc -DF_CPU=16000000 -mmcu=atmega4809 -Os -x assembler -nostartfiles -g -o ./Beep182/beep182.arduino.megaavr.nona4809.elf ./Beep182/main.S &&
avr-objcopy -O ihex -R .eeprom ./Beep182/beep182.arduino.megaavr.nona4809.elf ./Beep182/beep182.arduino.megaavr.nona4809.hex &&
arduino-cli upload -p /dev/cu.usbmodem14101 --fqbn arduino:megaavr:nona4809 ./Beep182
```
